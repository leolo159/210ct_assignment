package com.company;

import java.util.Scanner;

public class CheckSubString {


        public static void main(String[] args) {
            Scanner Scan = new Scanner(System.in);
            System.out.print("Enter the first String: ");
            String firstString = Scan.nextLine();
            System.out.println("");
            System.out.print("Enter the second String: ");
            String secondString =  Scan.nextLine();

            checkSubString(firstString,secondString);

        }

        public static void checkSubString(String firstString, String secondString){
            System.out.print(secondString + " is");
            if(!firstString.contains(secondString)){
                System.out.print(" not");
            }
            System.out.print(" a subString of "+ firstString);
        }

}
