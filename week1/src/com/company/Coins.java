package com.company;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Coins {
    static double coinTypes[] = {0.1, 0.2, 0.5, 1.0, 2.0,
            5.0, 10.0};
    static int n = coinTypes.length;

    public static void main(String[] args) {
        double inputNum = 0;
        Scanner Scan = new Scanner(System.in);
        System.out.print("Enter a number: ");
        try {
            inputNum = Scan.nextDouble();
        } catch (Exception e) {
            System.out.println("Please enter a Number\nTry again!");
            return;
        }

        System.out.print(
                "the minimum number of coins to make up HK$" + inputNum + " : \n");
        findMin(inputNum);
    }


    static void findMin(double V) {
        // Initialize result
        ArrayList<Double> ans = new ArrayList<Double>();

        // Traverse through all denomination
        for (int i = n - 1; i >= 0; i--) {
            // Find denominations
            while (V >= coinTypes[i]) {
                V = BigDecimal.valueOf(V).subtract(BigDecimal.valueOf(coinTypes[i])).doubleValue();
                ans.add(coinTypes[i]);
            }
        }

        // Print result
        for (int i = n - 1; i > 0; i--) {
            int occurrences = Collections.frequency(ans, coinTypes[i]);
            if (coinTypes[i] >= 1) {
                System.out.print(occurrences + " x " + (int) coinTypes[i] + " dollar coin, ");
            } else {
                System.out.print(occurrences + " x " + (int) (coinTypes[i] * 100) + " cents coins.");
            }
        }
    }
}
