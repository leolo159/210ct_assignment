// Definition of abstract base class Shape
public abstract class Shape {

  protected String name;

  public Shape(String n) {
    name = new String(n);
  }

  public abstract double getArea();

  public String getName() {
    return name;
  }

}

class Circle extends Shape {

  private Point center;
  private double radius;

  Circle(double pointA, double pointB, double radius) {
    super("Circle");
    this.center = new Point(pointA, pointB);
    this.radius = radius;
  }

  public Point getCenter() {
    return center;
  }

  public void setCenter(Point center) {
    this.center = center;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double radius) {
    this.radius = radius;
  }

  @Override
  public String toString() {
    return "Circle [center=" + center + ", radius=" + radius + "]";
  }

  @Override
  public double getArea() {
    // TODO Auto-generated method stub
    return radius * radius *Math.PI;
  }

}

class Rectangle extends Shape {
  private Point topLeft;
  private double length;
  private double width;

  public Rectangle(double pointA, double pointB, double length, double width) {
    super("Rectangle");
    this.topLeft = new Point(pointA, pointB);
    this.length = length;
    this.width = width;
  }

  public Point getTopLeft() {
    return topLeft;
  }

  public void setTopLeft(Point topLeft) {
    this.topLeft = topLeft;
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  @Override
  public String toString() {
    return "Rectangle [length=" + length + ", topLeft=" + topLeft + ", width=" + width + "]";
  }

  @Override
  public double getArea() {
    // TODO Auto-generated method stub
    return width * length;
  }

}
