package com.company;

import java.util.Scanner;

public class FibonacciSeriesRecursion {
    public static void main(String[] args){
        System.out.println("First 10 Fibonacci numbers are :");
        for(int num = 1; num<= 10; num++){
            System.out.println("Fibonacci(" +num + ") = " + fib(num));
        }
        System.out.println("*********************************************\n");
        //input to print Fibonacci series upto how many numbers
        System.out.println("Enter number upto which Fibonacci series to print: ");
        int number = new Scanner(System.in).nextInt();
        System.out.println("Fibonacci series upto " + number +" numbers : ");
        //printing Fibonacci series up to number
        for(int i=1; i<=number; i++){
            System.out.print(fib(i) +" ");
        }
    }

    /*
     * Java program for Fibonacci number using recursion.
     * This program uses tail recursion to calculate Fibonacci number for a given number
     * @return Fibonacci number
     */
    public static int fib(int num){
        if(num == 1 || num ==2){ //base case
            return 1;
        } else{
            return fib(num-1) + fib(num-2);////tail recursion
        }
    }
}
