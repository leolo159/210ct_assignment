package com.company;

import java.util.Scanner;

public class ArrayQueueTest {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args){
        ArrayQueue q = null;
        int choice = 0;
        char ch = ' ';
        System.out.println("\nInterview waiting list Operations(FIFO)\n");
        String qSize = "Enter the size of the waiting list(Press enter to use default size):";
        q = checkSize(qSize,q);
        do{
            System.out.println("\n1. Insert the name of waiting");
            System.out.println("2. Remove the fist in the waiting list");
            System.out.println("3. Check the waiting is full");
            System.out.println("4. Check how many people is waiting");
            System.out.println("5. Check the array queue is Empty");
            System.out.println("6. Get the full waiting list");
            System.out.print("Enter Choice : ");
            choice = sc.nextInt();
            System.out.println();
            switch (choice)
            {
                case 1 :
                    System.out.print("Enter name: ");
                    String name = sc.next();
                    q.enQueue(name);
                    break;
                case 2 :
                    System.out.println("----The Front person is " + "\""+ q.front() +"\"" + "----");
                    while(!q.isEmpty()){
                        System.out.println("Removing:" + "\""+ q.front() + "\"");
                        q.deQueue();
                        break;
                    }
                    break;
                case 3 :
                    System.out.println("Full status = "+q.isFull());
                    break;
                case 4 :
                    System.out.print(q.size()+" people(s)");
                    break;
                case 5 :
                    System.out.println("Empty status = "+q.isEmpty());
                    break;
                case 6 :
                    break;
                default :
                    System.out.println("Wrong Entry \n ");
                    break;
            }
            System.out.println(q);

            System.out.print("\nDo you want to continue (Type y or n)");
            ch = sc.next().charAt(0);
        } while (ch == 'Y'|| ch == 'y');
    }

    public static ArrayQueue checkSize(String Question,ArrayQueue q){
        System.out.print(Question);
        String size = sc.nextLine();
        if(!size.isEmpty() && size != null){
            q = new ArrayQueue(Integer.parseInt(size));
        }else{
            q = new ArrayQueue();
        }
        return q;
    }
}
