package com.company;

import java.util.Arrays;

public class BubbleSort {
    public static int sortingTime = 1;
    public static void main(String [ ] args) {
        int [ ] arr = {8,38,93,39,21,26,63,11,32};
        System.out.println("Original array is: " + Arrays.toString(arr));
        bubbleSort(arr);
        System.out.print("Sorted array is (Bubble sort): [ ");
        for (int i=0; i<arr.length; i++)
            System.out.print(arr[i] + " ");
        System.out.println("]");
    }

    public static void bubbleSort(int [] array) {
        int n = array.length;
        for(int pass=0; pass<n-1; pass++){
            for(int a=0; a<n-pass-1; a++) {
                if(array[a] > array[a+1]){
                    swap(array,a,a+1);
                }
                System.out.print("Sorting array is (Bubble sort) "+sortingTime+++" :[ ");
                for (int i=0; i<array.length; i++)
                    System.out.print(array[i] + " ");
                System.out.println("]");
            }
        }
    }

    public static void swap (int [ ] array, int first, int second) {
        int temp = array [ first ];
        array [ first ] = array [ second ];
        array [ second ] = temp;
    }
}
