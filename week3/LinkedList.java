class ListNode {
	private Object data;
	private ListNode next;
	ListNode(Object o) { data = o; next = null; }
	ListNode(Object o, ListNode nextNode) 
		{ data = o; next = nextNode; }

	public void setData(Object data) {
		this.data = data;
	}

	public void setNext(ListNode next) {
		this.next = next;
	}

	Object getData() { return data; }
	ListNode getNext() { return next; }
} // class ListNode

class EmptyListException extends RuntimeException {
	public EmptyListException () { super ("List is empty"); }
} // class EmptyListException

class LinkedList {
	private ListNode head;
	private ListNode tail;
	public LinkedList() { head = tail = null; }
	public boolean isEmpty() { return head == null; }
	public void addToHead(Object item) {
		ListNode newnode = new ListNode(item);
		if (isEmpty()) {
			head = tail = new ListNode(item);
		}
		else {
			head = new ListNode(item, head);
			tail.setNext(newnode);
		}


	}

	public void addToTail(Object item) {
		ListNode newNode = new ListNode(item);
		if(isEmpty()){
			head=tail= new ListNode(item);
		}
		else {

			tail.setNext(newNode);
			tail = tail.getNext();
			tail.setNext(new ListNode(head.getData()));

			//tail = tail.getNext();

		}

	}

	public Object removeFromHead() throws EmptyListException {
		Object item = head.getData();
		if(head==tail)
			head=tail=null;
		else {
			head = head.getNext();
			tail.setNext(new ListNode(head.getData()));
		}

		return item;
	}

	public Object removeFromTail() throws EmptyListException {
		Object item = null;
		if (isEmpty())
			throw new    EmptyListException();
		item =tail.getData();
		if (head==tail)
			head=tail=null;
		else
		{
			ListNode current =head;
			while(current.getNext()!=tail)
				current=current.getNext();
			current.setNext(new ListNode(head.getData()));
			tail=current;


		}
		return item;
	}

	public String toString () {
		String s = "[ ";
		ListNode current = head;
		while (current != null) {
			s += current.getData() + " ";
			current = current.getNext();
		}
		return s + "]";
	}
} // class LinkedList
