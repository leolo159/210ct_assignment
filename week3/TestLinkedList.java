import java.util.Scanner;

/**
 * Title:           TestLinkedList.java
 * Description:     A simple test drive program for the LinkedList class
 * Company:         ICT HKIVE(TY)
 * @author
 */

public class TestLinkedList {
	public static void main (String args[]) {
		LinkedList s = new LinkedList();
		Scanner input = new Scanner(System.in);
		char ch = ' ';
		int choice = 0;
		/*  Perform list operations  */
		do{
			System.out.println("\nCircular Linked List Operations\n");
			System.out.println("1. Insert at begining");
			System.out.println("2. Insert at end");
			System.out.println("3. Remove form the first node");
			System.out.println("4. Remove form the last node");
			System.out.println("5. Check is circular linked list");
			System.out.println("6. Get the Linked List");
			System.out.println("7. Auto Test");
			System.out.print("Enter Choice : ");
			choice = input.nextInt();
			System.out.println();
			switch (choice)
			{
				case 1 :
					System.out.print("Enter Number or String element to insert at beginning: ");
					s.addToHead( input.next() );
					break;
				case 2 :
					System.out.print("Enter Number or String element to insert at end: ");
					s.addToTail( input.next() );
					break;
				case 3 :
					System.out.print("Removing...");
					s.removeFromHead();
					break;
				case 4 :
					System.out.print("Removing...");
					s.removeFromTail();
					break;
				case 5 :
					System.out.println("Empty status = ");
					break;
				case 6 :
					System.out.println(s);
					break;

				case 7:
					s.addToTail (new Integer(42));
					System.out.println(s);
					System.out.println();
					s.addToTail (new Character('n'));
					System.out.println(s);
					System.out.println();
					s.addToTail (new String("hello"));
					System.out.println(s);
					System.out.println();
					while (!s.isEmpty()) {
						System.out.println("removed:" + s.removeFromHead());
						System.out.println(s);
						System.out.println();
					}

					s.addToHead (new Integer(42));
					System.out.println(s);
					System.out.println();
					s.addToHead (new Character('n'));
					System.out.println(s);
					System.out.println();
					s.addToHead (new String("hello"));
					System.out.println(s);
					System.out.println();
					while (!s.isEmpty()) {
						System.out.println("removed:" + s.removeFromTail());
						System.out.println(s);
						System.out.println();
					}
					break;
				default :
					System.out.println("Wrong Entry \n ");
					break;
			}
			System.out.println();
			System.out.println(s);
			/*  Display List  */
			System.out.print("\nDo you want to continue (Type y or n)");
			ch = input.next().charAt(0);
		} while (ch == 'Y'|| ch == 'y');
		System.out.print("---------Circular Linked List Operations OFF-----------");
	}
} // class TestLinkedList