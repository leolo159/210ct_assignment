package com.company;

public class DoublyNode {
    String data;
    DoublyNode previous;
    DoublyNode next;

    public DoublyNode(){
        this(null,null,null);
    }

    public DoublyNode(String data){
        this(data,null,null);
    }

    public DoublyNode(String data,DoublyNode previous,DoublyNode next){
        this.data = data;
        this.previous = previous;
        this.next = next;
    }
}
