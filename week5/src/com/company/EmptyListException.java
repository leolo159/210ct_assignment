package com.company;

public class EmptyListException extends NullPointerException {
    public EmptyListException(){
        super("List is Empty");
    }
}
