package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String [] args) {
        DoublyList d = new DoublyList();
        ArrayList<String> words = new ArrayList<String>();
        String text = readFile("/Users/leolo/Desktop/week5/src/com/company/A-paragraph.txt");
        words = getWords(words, text);
        Collections.sort(words);
        for (String word : words) {
            d.insertAtRightSpot(word);
        }
        d.print();
        System.out.println("\n\n"+d);
    }

    public static String readFile(String fileName){
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = br.readLine()) != null) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        String contents = sb.toString();
        return contents;
    }

    public static ArrayList<String> getWords(ArrayList<String> words, String text) {

        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(text);
        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIndex))) {
                words.add(text.substring(firstIndex, lastIndex));
            }
        }

        return words;
    }
}
