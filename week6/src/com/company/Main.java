package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static Scanner s = new Scanner(System.in);

    public static void main(String args[])
    {
        binarySearchRecursive ob = new binarySearchRecursive();
        int[] arr = new int[5];
        int n = arr.length;
        String Question = "Generating 5 integer number...";
        arr = genNumber(Question,arr);
        String Question1 = "Enter the number you want to search...";
        int num = checkNumber(Question1);
        //int n = arr.length - 1;
        int result = ob.binarySearch(arr, 0, n - 1, num); ;
        if (result != -1) {
            System.out.println("looking " + num + " in array..." + Arrays.toString(arr));
            System.out.println("Element found at index " + result);

        }else{
            System.out.println("Sorry, number "+ num +" not found in array");
        }
    }

    public static int[] genNumber(String Question, int[] arr){
        Random rand = new Random();
        int number = 0;
        int index = 0;
        System.out.println(Question);
        while(index < arr.length){
            number = (int)(Math.random()*100 + 1);
            System.out.println("#Number" + index +": " + number);
            arr[index] = number;
            index++;
        }
        return arr;
    }

    public static int checkNumber(String Question){
        int number = 0;
        System.out.println(Question);
        boolean flag = false;
        while(!flag){
            try{
                number = Integer.parseInt(s.nextLine());
                if (number < 1)
                    throw new ArithmeticException("Must be an integer of one or more.");
                else{
                    flag = true;
                }
            }catch(NumberFormatException e){
                System.err.println("Wrong input! Input only integer numbers please: " + e.getMessage());
            }catch(ArithmeticException ex){
                System.err.println(ex.getMessage());
            }
        }
        return number;
    }
}
